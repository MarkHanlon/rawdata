const fs = require('fs')
const { stringify } = require('querystring')

const inputFolder = '/data/inputs'
const outputFolder = '/data/outputs'
//const inputFolder = '.'
//const outputFolder = '.'

async function countrows(file) {
  console.log('Start counting for ' + file)
  const fileBuffer = fs.readFileSync(file)
  const toString = fileBuffer.toString()
  const splitLines = toString.split('\n')
  const rows = splitLines.length - 1
  fs.appendFileSync(outputFolder + '/output.log', file + ', ' + rows + ' rows\r\n')
  console.log('Finished. We have ' + rows + ' lines')
}

async function calcEnergy(file) {
  console.log('Start calculating energy total for ' + file)
  const fileBuffer = fs.readFileSync(file)
  const toString = fileBuffer.toString()
  const splitLines = toString.split('\n')

  let totalWh = 0
  splitLines.forEach(li => {
    if (li.indexOf(',' ) > -1) {
      const cols = li.split(',')
      totalWh += cols[1] / 360.0
    }
  });
  
  fs.appendFileSync(outputFolder + '/output.log', file + ', ' + totalWh + 'Wh total\r\n')
  console.log('Finished. We have ' + totalWh + 'Wh total energy')
}

async function processfolder(folder) {
  const files = fs.readdirSync(folder)

  for (let i = 0; i < files.length; i++) {
    const file = files[i]
    const fullpath = folder + '/' + file
    if (fs.statSync(fullpath).isDirectory()) {
      await processfolder(fullpath)
    } else {
      await countrows(fullpath)
    }
  }
}

processfolder(inputFolder)
